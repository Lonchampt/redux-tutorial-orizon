import React from 'react'
import '../styles/app.css'
import ToDoList from './ToDoList'
import PropTypes from 'prop-types'

const App = () => {
  const AppStyle = {
    backgroundColor: '#333',
    height: '100%',
    width: '100%',
    color: '#EEE',
    paddingTop: 10,
  };

  const HeaderStyle = {
    margin: '20px 0px',
    textAlign: 'center',
  };

  return (
      <div style={AppStyle}>
        <div style={HeaderStyle}>
          <h1>ToDoList</h1>
          <blockquote>Noooon, pas encore ! <strong>  :'(</strong> </blockquote>
          <hr/>
        </div>
        <ToDoList></ToDoList>
      </div>
  )
};

App.propTypes = {

}

export default App
