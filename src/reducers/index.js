/***************************************
** O-rizon development
** Created by Bastien Cecere
** 18/04/2017 - 19:53
** index.js.js
** 2017 - All rights reserved
***************************************/

import { combineReducers } from 'redux'
import ToDos from './ToDo'

const ToDoApp = combineReducers({
	ToDos,
})

export default ToDoApp